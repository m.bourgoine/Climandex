package com.cow.climandexapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.Random;

public class Home extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }


    public void index(View view){
        Intent intent = new Intent(this, Index.class);
        startActivity(intent);
    }

    public void qf(View view){
        Intent intent = new Intent(this, QF.class);
        startActivity(intent);
    }


    public void grp(View view){
        Intent intent = new Intent(this, Group.class);
        startActivity(intent);

        MediaPlayer moo = MediaPlayer.create(getApplicationContext(), R.raw.moo);
        moo.start();

    }

}